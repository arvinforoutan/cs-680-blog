<?php

class Post extends Eloquent {

    protected $fillable = array('title');

    public function text()
    {
        return $this->hasOne('Text');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function author()
    {
        return $this->belongsTo('User', 'author_id');
    }

    public function tags()
    {
        return $this->belongsToMany('Tag');
    }

}