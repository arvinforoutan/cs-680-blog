<?php

class Comment extends Eloquent {

	protected $fillable = array('text');

    public function post()
    {
        return $this->belongsTo('Post');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}