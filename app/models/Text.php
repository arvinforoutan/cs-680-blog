<?php

class Text extends Eloquent {

    protected $fillable = array('text');

    public function post()
    {
        return $this->belongsTo('Post');
    }

}