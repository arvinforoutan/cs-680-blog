<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	protected $fillable = array('name');
	protected $hidden = array('password');

	public function posts()
	{
	    return $this->hasMany('Post', 'author_id');
	}

	public function tags()
	{
	    $tags = array();

	    $rows = Tag::select('tags.id', 'tags.name')
	               ->join('post_tag', 'tags.id', '=', 'post_tag.tag_id')
	               ->join('posts', 'post_tag.post_id', '=', 'posts.id')
	               ->where('posts.author_id', $this->id)
	               ->groupBy('tags.id')
	               ->orderBy('tags.name')
	               ->get();

	    foreach ($rows as $row) {
	        $tags[] = $row;
	    }

	    return $tags;
	}

	public function comments()
	{
	    return $this->hasMany('Comment');
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

}