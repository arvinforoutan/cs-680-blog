<?php

class Tag extends Eloquent {

    protected $fillable = array('name');

    public function posts()
    {
        return $this->belongsToMany('Post');
    }

}