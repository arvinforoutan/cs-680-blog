@extends('layouts.blog');

@section('title')
Create Post...
@stop

@section('content')
<div class="row">
    @if($errors->any())
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Please address the following errors</strong>
        <ul class="errors">
            @foreach($errors->all('<li>:message</li>') as $message)
            {{ $message }}
            @endforeach
        </ul>
    </div>
    @endif
    {{ Form::open(array('url' => 'posts')) }}
    <legend>New Blog Post</legend>

    <label for="title">Post Title</label>
    <div class="form-group">
        <input autofocus required type="text" class="form-control"
               name="title" placeholder="Enter a title.."
               value="{{Input::old('title')}}">
    </div>

    <label for="text">Post Tag(s)</label>
    <div class="form-group">
        <input type="text" class="form-control" name="tags" 
               placeholder="Enter tags, separated by commas.." 
               value="{{Input::old('tags')}}">
    </div>

    <label for="text">Post Text</label>
    <div class="form-group">
        <textarea required class="form-control" rows="10"
                  name="text" placeholder="Enter your text..">{{ Input::old('text') }}</textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Submit">
    {{ Form::close() }}
</div>
@stop

