@extends('layouts.blog')

@section('content')
    <section class="post">
        <header class="page-header">
            <h1>
                {{{ $post->title }}}<br>
                <small>By {{{ $post->author->first_name }}} 
                          {{{ $post->author->last_name }}}</small>
            </h1>
            <br>
            <div class="blog-post-meta">
                Tags: |
                @foreach ($post->tags as $tag)
                    {{ link_to('/tags/' . Str::lower($tag->name), Str::upper($tag->name)) }} |
                @endforeach
            </div>

            <div class="blog-post-meta">
                <span class="date">
                    Posted: {{ $post->created_at->format('M d, Y') }}
                </span>
            </div>
        </header>

        <p>{{ nl2br($post->text->text) }}</p>
    </section>

    <section class="comments">
        <header>
            <h3>Comments
            	<a href="{{ $post->id }}/comments" 
                   class="btn btn-primary">Add Comment</a>
            </h3>
        </header>

        @if (sizeof($post->comments) > 0)
	        @foreach ($post->comments as $comment)
	            <div class="row">
	                <div class="col-xs-12">
	                    {{{ $comment->user->first_name }}} 
                        {{{ $comment->user->last_name }}} 
	                    wrote on 
                        {{{ $comment->created_at->format('M d, Y g:i:s A') }}}:
	                    <blockquote>{{{ $comment->text}}}</blockquote>
	                </div>
	            </div>
	        @endforeach
	    @else
	    	<em>No comments yet.</em>
	    @endif
    </section>
@stop