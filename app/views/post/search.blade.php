@extends('layouts.blog')

@section('content')
    <div class="row">
    	<h2>Search Results<hr></h2>
    	@if (sizeof($texts) === 0)
    		<h4>No results found.</h4>
    	@else
	        @foreach ($texts as $text)
	        	@if (sizeof($text->post) > 0)
		            <div class="col-xs-12">
		                <h3>
		                    <a href="{{ $text->post->id }}">{{ $text->post->title }}</a>
		                </h3>
		            </div>
	            @endif
	        @endforeach
        @endif
    </div>
@stop