@extends('layouts.blog')

@section('content')

@if(sizeof($posts) === 0)
    <h3>No Posts Found</h3>
@else
<table class="table table-condensed">
    <thead>
    <tr>
        <th class="col-xs-2">Actions</th>
        <th class="col-xs-7">Title</th>
        <th class="col-xs-3">Date Created</th>
    </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
    <tr>
        <td class="col-xs-2">
            <button class="btn btn-danger" data-toggle="modal" data-target=".confirm-delete">
                <span class="glyphicon glyphicon-trash"></span>
            </button>

            <a href="{{$post->id}}/edit" class="btn btn-primary">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
        </td>

        <td class="col-xs-7"> {{ $post->title}}</td>
        <td class="col-xs-3"> {{ $post->created_at->format('M d, Y') }}</td>
    </tr>
    @endforeach
    </tbody>
</table>

<div class="modal fade confirm-delete">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete?</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete {{ $post->title }}?</p>
            </div>
            <div class="modal-footer">
                {{ Form::open(array('route' => array('posts.destroy', $post->id), 'method' => 'delete')) }}
                    <button class="btn btn-success" href="{{ URL::route('posts.destroy', $post->id) }}">Yes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@endif
@stop