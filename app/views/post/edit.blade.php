@extends('layouts.blog');

@section('title')
Edit Post...
@stop

@section('content')
<div>
    {{ Form::open(array('url' => 'posts/' . $post->id, 'method'=>'put')) }}
    <legend>Edit Blog Post</legend>

    <label for="title">Post Title</label>
    <div class="form-group">
        <input autofocus required type="text" class="form-control"
               name="title" placeholder="Enter a title.." value="{{ $post->title }}">
    </div>

    <label for="text">Post Tag(s)</label>
    <div class="form-group">
        <input required type="text" class="form-control"
               name="tags" placeholder="Tags" 
               value="{{ implode(', ', $post->tags->lists('name')) }}">
    </div>

    <label for="text">Post Text</label>
    <div class="form-group">
        <textarea required class="form-control" rows="10" name="text"
                  placeholder="Enter your text..">{{ $post->text->text }}</textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Update">
    {{link_to('posts/manage', 'Cancel', array('class'=>'btn btn-danger')) }}

    {{ Form::close() }}
</div>
@stop

