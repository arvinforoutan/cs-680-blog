@extends('layouts.blog')
@section('title')
Home
@stop

@section('content')
{{ Form::open(array('url' => 'posts/search', 'method' => 'POST', 'class' => 'form-inline search')) }}
    <div class="form-group">
            <input type="text" id="search-input" class="form-control" 
                   name="query" placeholder="Search the site..">
    <input type="submit" class="btn btn-primary" value="Search">
    </div>
{{ Form::close() }}
@if(sizeof($posts) === 0)
<h3>No Posts Found.</h3>
@else
<div class="page-header">
    <h1>
        Recent Posts
    </h1>
</div>
@foreach($posts as $post)
<div class="row">
    <div class="col-md-12">
        <h3>
            <a href="posts/{{ $post->id }}">{{ $post->title }}</a>
            <br>
            <small>
                By
                {{ $post->author->first_name }}
                {{ $post->author->last_name }}
                on
                {{ $post->created_at->format('M d, Y') }}
            </small>
        </h3>

        <blockquote>
            {{ nl2br(Str::words($post->text->text, 80)) }}
        </blockquote>
    </div>
</div>
@endforeach
@endif
@stop