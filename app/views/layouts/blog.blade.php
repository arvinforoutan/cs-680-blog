@extends('layouts.master')

@section('body')
<div class="row">
    <div class="col-md-9">
        @yield('content')
    </div>

    <div class="col-md-2 col-md-offset-1">
        <!-- Post management -->
        @if (Auth::check())
        <div class="panel panel-primary">
            <div class="panel-heading">
                Post Menu
            </div>
            <ul class="list-group">
                {{ HTML::listGroupLink('/posts/create','New Post') }}
                {{ HTML::listGroupLink('/posts/manage','My Posts') }}
            </ul>
        </div>
        @endif

        <!--Popular Tags-->
        <div class="panel panel-primary">
            <div class="panel-heading">
                Popular Tags
            </div>
            <ul class="list-group">
                @foreach($tags as $tag)
                    {{ link_to('/tags/' . $tag->name, $tag->name, 
                        $attributes = array('class' => 'list-group-item')) }}
                @endforeach
            </ul>
        </div>
    </div>
</div>
@stop