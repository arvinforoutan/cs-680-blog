<!DOCTYPE html>
<html>
    <head>
        {{ HTML::style("//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css") }}
        {{ HTML::style('css/site.css') }}
        <title>@yield('title')</title>
    </head>

    <body>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{ link_to("/", "Blog", array("class" => "navbar-brand")) }}

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        {{ HTML::navBarLink('/','Home') }}
                    </ul>
                    @if (Auth::check())

                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    {{Auth::user()->first_name}}
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>{{ link_to('#', 'View Profile') }}</li>
                                    <li>{{ link_to('#', 'Account') }}</li>
                                    <li class="divider"></li>
                                    <li>{{ link_to('logout', 'Logout') }}</li>
                                </ul>
                            </li>
                        </ul>
                    @else
                        <ul class="nav navbar-nav navbar-right">
                            {{ HTML::navBarLink('register','Register') }}
                            {{ HTML::navBarLink('login','Login') }}
                        </ul>
                    @endif
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">
            @section('body')
            @show
        </div>

        {{ HTML::script("//code.jquery.com/jquery-1.11.0.min.js") }}
        {{ HTML::script("//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js") }}
    </body>
</html>