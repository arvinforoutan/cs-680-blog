@extends('layouts.master')

@section('title')
Register
@stop

@section('body')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        @if($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Please address the following errors</strong>
            <ul class="errors">
                @foreach($errors->all('<li>:message</li>') as $message)
                {{ $message }}
                @endforeach
            </ul>
        </div>
        @endif
        <form action="register" method="POST" role="form" class="form-signin">
            <legend>Register</legend>

            <label for="email">Email Address</label>
            <div class="form-group">
                <input required autofocus type="text" class="form-control"
                       name="email" placeholder="Email Address"
                       value="{{Input::old('email')}}" >
            </div>

            <label for="first_name">First Name</label>
            <div class="form-group">
                <input required type="text" class="form-control"
                       name="first_name" placeholder="First Name"
                       value="{{Input::old('first_name')}}">
            </div>

            <label for="last_name">Last Name</label>
            <div class="form-group">
                <input required type="text" class="form-control"
                       name="last_name" placeholder="Last Name"
                       value="{{Input::old('last_name')}}">
            </div>

            <label for="password">Password</label>
            <div class="form-group">
                <input required type="password" class="form-control"
                       name="password" placeholder="Password">
            </div>

            <input class="btn btn-success btn-lg btn-block"
                   type="submit" value="Register">
        </form>
        <div class="center">
            Already a user? <a href="login">Sign in.</a>
        </div>
    </div>
</div>
@stop