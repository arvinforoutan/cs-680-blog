@extends('layouts.master')

@section('title')
Login
@stop

@section('body')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div>
            @if($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Please address the following errors</strong>
                <ul class="errors">
                    @foreach($errors->all('<li>:message</li>') as $message)
                    {{ $message }}
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <form method="POST" action="login" class="form-signin" role="form">
            <legend>Login</legend>

            <label for="email">Email Address</label>
            <div class="form-group">
                <input required autofocus type="text" class="form-control"
                       name="email" placeholder="Email Address" value="{{Input::old('email')}}"></p>
            </div>

            <label for="password">Password</label>
            <div class="form-group">
                <input required type="password" class="form-control"
                       name="password" placeholder="Password"></p>
            </div>

            <input class="btn btn-primary btn-lg btn-block"
                   type="submit" value="Login">
        </form>
        <div class="center">
            Don't have an account? <a href="register">Sign up.</a>
        </div>
    </div>
</div>
@stop