@extends('layouts.blog');

@section('title')
	Add Comment...
@stop

@section('content')
<div>
    {{ Form::open(array('url' => 'posts/' . $route . '/comments')) }}
        <legend>Add Comment</legend>

        <div class="form-group">
            <textarea autofocus required class="form-control" 
            		  rows="10" name="text" placeholder="Enter your comment.."></textarea>
        </div>

        <input type="submit" class="btn btn-primary" value="Submit">
    {{ Form::close() }}
</div>
@stop

