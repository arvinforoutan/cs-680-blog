<?php

class UserTableSeeder extends Seeder {

    public function run()
    {   
        DB::table('users')->delete();

        User::create(array(
            'email'             => 'arvin.foroutan@gmail.com',
            'first_name'        => 'Arvin',
            'last_name'         => 'Foroutan',
            'password'          => Hash::make('arvin')
        ));

        User::create(array(
            'email'             => 'adrianhaurat@gmail.com',
            'first_name'        => 'Adrian',
            'last_name'         => 'Haurat',
            'password'          => Hash::make('adrian')
        ));

        User::create(array(
            'email'             => 'rxvcgiii@gmail.com',
            'first_name'        => 'Chris',
            'last_name'         => 'Eng',
            'password'          => Hash::make('chris')
        ));

        User::create(array(
            'email'             => 'devonsmart90@gmail.com',
            'first_name'        => 'Devon',
            'last_name'         => 'Smart',
            'password'          => Hash::make('devon')
        ));
    }

}