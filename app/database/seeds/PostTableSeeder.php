<?php

class PostTableSeeder extends Seeder {

    public function run()
    {   
        DB::table('posts')->delete();

        Post::create(array(
            'title'         => 'Welcome to the COMP 680 blog!',
            'author_id'     => 1
        ));

        $post = Post::find(1);

        $text = new Text;
        $text->text = 'Great work team, we did it!';
        $text->save();

        $tag1 = Tag::create(array('name' => 'PHP'));
        $tag2 = Tag::create(array('name' => 'Laravel'));
        $tag3 = Tag::create(array('name' => 'MVC'));

        $comment1 = new Comment;
        $comment1->text = 'Nice job everyone!';
        $comment1->post_id = $post->id;
        $comment1->user_id = 2;
        $comment1->save();

        $comment2 = new Comment;
        $comment2->text = 'Awesome!';
        $comment2->post_id = $post->id;
        $comment2->user_id = 3;
        $comment2->save();

        $comment3 = new Comment;
        $comment3->text = 'Very cool!';
        $comment3->post_id = $post->id;
        $comment3->user_id = 4;
        $comment3->save();


        $post->text()->save($text);
        $post->comments()->save($comment1);
        $post->comments()->save($comment2);
        $post->comments()->save($comment3);
        $post->tags()->save($tag1);
        $post->tags()->save($tag2);
        $post->tags()->save($tag3);
    }

}