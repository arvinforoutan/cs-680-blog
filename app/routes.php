<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('posts/manage', 'PostController@getManage');
/* Automatically handles RESTful routes */
Route::resource('posts', 'PostController');
Route::resource('tags', 'TagController');

Route::get('posts/{id}/comments', 'PostController@addComment');
Route::post('posts/{id}/comments', 'PostController@saveComment');

Route::post('posts/search', 'PostController@searchPosts');

Route::get('/', 'HomeController@showIndex');

Route::get('login', 'AuthController@getLogin');
Route::post('login', 'AuthController@postLogin');

Route::get('register', 'AuthController@getRegister');
Route::post('register', 'AuthController@postRegister');

Route::get('logout', 'AuthController@getLogout');