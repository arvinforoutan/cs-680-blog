<?php

class TagController extends \BaseController {

    protected $layout = 'layouts.master';

    public function show($name)
    {
        $tag = Tag::with('posts')->where('name', 'like', $name)->first();

        $this->layout->content = View::make('tag.show')->with('tag', $tag);
    }

}