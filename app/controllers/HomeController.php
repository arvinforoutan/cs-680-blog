<?php

class HomeController extends BaseController {

	public function showIndex()
	{
		$posts = Post::with('text')->orderBy('created_at', 'desc')->get();
        return View::make('index')->with('posts', $posts);
	}


}