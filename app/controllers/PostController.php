<?php

class PostController extends \BaseController {

    protected $layout = 'layouts.master';

    public function __construct()
    {
        $this->beforeFilter('auth', array('except'=> 'show'));
    }

    public function create()
    {
        $this->layout->content = View::make('post.create');
    }

    public function store()
    {
        $input = Input::all();

        $rules = array(
            'title'  => 'required',
            'text'   => 'required',
            'tags'   => 'required'
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        $post = Post::create(array('title' => $input['title']));

        $text = Text::create(array('text' => $input['text']));

        $post->text()->save($text);

        $tags = array_map('trim', explode(',', $input['tags']));

        foreach ($tags as $tag) {
            try {
                $tag = Tag::where('name', $tag)->firstOrFail();
            } catch (Exception $e) {
                $tag = Tag::create(array('name' => $tag));
            }
            $post->tags()->save($tag);
        }

        $post->author()->associate(Auth::user())->save();

        return Redirect::to('/');
    }

    public function show($id)
    {
        $post = Post::find($id);
        $post->load('text', 'author', 'tags', 'comments');

        $this->layout->content = View::make('post.show')->with('post', $post);
    }

    public function addComment($route)
    {
        $this->layout->content = View::make('comment.create')->with('route', $route);
    }

    public function saveComment($id)
    {
        $input = Input::all();
        $rules = array( 'text' => 'required');

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Redirect::action('PostController@addComment')->withErrors($validation);
        }

        $post = Post::findOrFail($id);

        $comment = new Comment;
        $comment->text = $input['text'];
        $comment->post_id = $post->id;
        $comment->user_id = Auth::user()->id;
        $comment->save();

        $post->comments()->save($comment);

        return Redirect::to('posts/' . $post->id);
    }

    public function getManage()
    {
        $posts = Post::where('author_id', '=', Auth::user()->id )->with('text')->orderBy('created_at', 'desc')->get();
        return View::make('post.manage', array('posts'=>$posts));
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $tags = array_map('trim', explode(',', $post->tags));

        return View::make('post.edit', array('post'=>$post));
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return Redirect::to('posts/manage');
    }

    public function update($id)
    {
        $input = Input::all();
        $rules = array(
            'title' => 'required',
            'text'  => 'required',
            'tags'  => 'required'
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Redirect::to('posts/' . $id)->withErrors();
        }

        $post = Post::findOrFail($id);
        $post->title = $input['title'];
        $text = $post->text;
        $text->text = $input['text'];
        $post->text()->save($text);
        $post->save();

        $input_tags = array_map('trim', explode(',', $input['tags']));
        $post_tags = $post->tags->lists('name');

        /* Add any new tags */
        foreach ($input_tags as $input_tag) {
            if (!in_array($input_tag, $post_tags)) {
                try {
                    $tag = Tag::where('name', $input_tag)->firstOrFail();
                } catch (Exception $e) {
                    $tag = Tag::create(array('name' => $input_tag));
                }
                $post->tags()->save($tag);
            }
        }

        /* Remove any tags that are no longer used */
        foreach($post_tags as $post_tag) {
            if (!in_array($post_tag, $input_tags)) {
                $tag = Tag::where('name', $post_tag)->first();
                $post->tags()->detach($tag->id);
            }
        }

        return Redirect::to('posts/' . $id);
    }

    public function searchPosts()
    {
        $input = Input::all();
        $rules = array('query' => 'required');

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Redirect::to('/')->withErrors($validation);
        }

        $texts = Text::with('post')->where('text', 'like', '%' . Str::lower($input['query']) . '%')->get();

        $this->layout->content = View::make('post.search')->with('texts', $texts);
    }
}