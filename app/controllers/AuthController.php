<?php

class AuthController extends BaseController {

	public function getLogin()
	{
		return View::make('auth.login');
	}

	public function getRegister()
	{
		return View::make('auth.register');
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}

	public function postLogin()
	{
		$userdata = array(
			'email'		=> Input::get('email'),
			'password'	=> Input::get('password')
		);

		$rules = array(
			'email'		=> 'required|email',
			'password'	=> 'required'		
		);

		$validator = Validator::make($userdata, $rules);

		if ($validator->fails()) {
			return Redirect::to('login')->withErrors($validator)->withInput();
		} else {
			if(Auth::attempt($userdata)){
				Log::info($userdata['email'] . ' has signed in.');
                return Redirect::to('/');
            }
            else{
                return Redirect::action('AuthController@getLogin')
                    ->with('message', 'Your username/password combination was incorrect')
                    ->withInput();
            }
		}

		return Redirect::intended('/');
	}

	public function postRegister()
	{
		$userdata = Input::all();

		$rules = array(
			'email'			=> 'required|email|unique:users',
			'first_name'	=> 'required|alpha', 
			'last_name'		=> 'required|alpha',
			'password'		=> 'required'
		);

		$validator = Validator::make($userdata, $rules);

		if ($validator->fails()) {
			return Redirect::to('register')->withErrors($validator)->withInput();
		} else {
			$user = new User();
			$user->email = $userdata['email'];
			$user->first_name = $userdata['first_name'];
			$user->last_name = $userdata['last_name'];
			$user->password = Hash::make($userdata['password']);
			$user->save();

			Log::info($user->email . ' has created an account.');
			Auth::attempt(Input::only('email', 'password'));
		}

		return Redirect::to('/');
	}
}